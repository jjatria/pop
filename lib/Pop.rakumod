use Pop::Singleton;
unit class Pop:ver<0.0.4>:auth<zef:jjatria> is Singleton;

use Pop::Graphics;
use Pop::Inputs;

has Bool    $.running;
has Instant $!last-frame;

# Callbacks
has &!update; method update ( &!update --> Nil ) { }
has &!render; method render ( &!render --> Nil ) { }

# Input callbacks
method key-pressed         ( &cb --> Nil ) { Pop::Inputs.key-pressed:         &cb }
method key-released        ( &cb --> Nil ) { Pop::Inputs.key-released:        &cb }
method mouse-moved         ( &cb --> Nil ) { Pop::Inputs.mouse-moved:         &cb }
method wheel-moved         ( &cb --> Nil ) { Pop::Inputs.wheel-moved:         &cb }
method mouse-pressed       ( &cb --> Nil ) { Pop::Inputs.mouse-pressed:       &cb }
method mouse-released      ( &cb --> Nil ) { Pop::Inputs.mouse-released:      &cb }
method controller-moved    ( &cb --> Nil ) { Pop::Inputs.controller-moved:    &cb }
method controller-pressed  ( &cb --> Nil ) { Pop::Inputs.controller-pressed:  &cb }
method controller-released ( &cb --> Nil ) { Pop::Inputs.controller-released: &cb }
method file-dropped        ( &cb --> Nil ) { Pop::Inputs.file-dropped:        &cb }
method text-dropped        ( &cb --> Nil ) { Pop::Inputs.text-dropped:        &cb }
method text-input          ( &cb --> Nil ) { Pop::Inputs.text-input:          &cb }
method text-edited         ( &cb --> Nil ) { Pop::Inputs.text-edited:         &cb }

# Window callbacks
method focused             ( &cb --> Nil ) { Pop::Inputs.focused:             &cb }
method mouse-focused       ( &cb --> Nil ) { Pop::Inputs.mouse-focused:       &cb }
method window-exposed      ( &cb --> Nil ) { Pop::Inputs.window-exposed:      &cb }
method window-moved        ( &cb --> Nil ) { Pop::Inputs.window-moved:        &cb }
method window-resized      ( &cb --> Nil ) { Pop::Inputs.window-resized:      &cb }
method window-visible      ( &cb --> Nil ) { Pop::Inputs.window-visible:      &cb }

submethod TWEAK (|c) {
    Pop::Graphics.new: |c;
    $!running = True;
}

method delta ( --> Duration ) { now - $!last-frame }

method stop ( --> Nil ) { $!running = False }

method wait ( Int $dt --> Nil ) {
    use Pop::SDL;
    SDL::Delay($dt)
}

method handle-input ( --> Nil ) {
    my $quit = Pop::Inputs.update;
    $!running = $quit if $!running;
}

method run ( :$fps = 60, :$dt is copy  --> Nil ) {
    my $frame-length = $fps ?? ( 1 / $fps ) !! 0;
    $dt //= $frame-length;

    $!last-frame = now;
    my $accumulator = 0;

    while $!running {
        LEAVE $!last-frame = ENTER now;

        $.handle-input;

        $accumulator += my $delta-time = $.delta;

        with &!update {
            my $read-time = $dt || $accumulator;
            while $accumulator >= $read-time {
                $accumulator -= $read-time;
                .( $read-time );
            }
        }

        if $delta-time <= $frame-length {
            with &!render { .(); Pop::Graphics.present }

            if $fps {
                my $delay = floor 1000 * ( $frame-length - $.delta );
                $.wait( $delay ) if $delay > 0;
            }
        }
    }
}

method destroy ( --> Nil ) {
    use Pop::Images;
    use Pop::Textures;

    Pop::Graphics.destroy;
    Pop::Images.destroy;
    Pop::Textures.destroy;
}
