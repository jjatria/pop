use Pop::Singleton;
class Pop::Inputs:ver<0.0.4>:auth<zef:jjatria> is Singleton {
    use Pop::SDL;
    use Pop::Config;

    my constant MOUSE-EVENT =
        | SDL::MOUSEMOTION
        | SDL::MOUSEBUTTONDOWN
        | SDL::MOUSEBUTTONUP
        | SDL::MOUSEWHEEL
    ;

    my constant KEYBOARD-EVENT =
        | SDL::KEYDOWN
        | SDL::KEYUP
        | SDL::TEXTEDITING
        | SDL::TEXTINPUT
        | SDL::KEYMAPCHANGED
    ;

    my constant JOYSTICK-EVENT =
        | SDL::JOYAXISMOTION
        | SDL::JOYBALLMOTION
        | SDL::JOYHATMOTION
        | SDL::JOYBUTTONDOWN
        | SDL::JOYBUTTONUP
        | SDL::JOYDEVICEADDED
        | SDL::JOYDEVICEREMOVED
    ;

    my constant CONTROLLER-EVENT =
        | SDL::CONTROLLERAXISMOTION
        | SDL::CONTROLLERBUTTONDOWN
        | SDL::CONTROLLERBUTTONUP
        | SDL::CONTROLLERDEVICEADDED
        | SDL::CONTROLLERDEVICEREMOVED
        | SDL::CONTROLLERDEVICEREMAPPED
        | SDL::CONTROLLERTOUCHPADDOWN
        | SDL::CONTROLLERTOUCHPADMOTION
        | SDL::CONTROLLERTOUCHPADUP
        | SDL::CONTROLLERSENSORUPDATE
    ;

    my constant DROP-EVENT =
        | SDL::DROPFILE
        | SDL::DROPTEXT
        | SDL::DROPBEGIN
        | SDL::DROPCOMPLETE
    ;

    my class Mouse {
        trusts Pop::Inputs;

        use Pop::Point;
        has Pop::Point $.xy    handles < x y >                  .= zero;
        has Pop::Point $.rel   handles ( dx => 'x', dy => 'y' ) .= zero;
        has Pop::Point $.wheel                                  .= zero;

        has Bool @!buttons;

        multi method button ( Int:D $key --> Bool ) {
            @!buttons[ $key - 1 ]
        }
        multi method button ( SDL::MouseButton $key --> Bool ) {
            @!buttons[ $key - 1 ]
        }
        multi method button ( Str:D $key --> Bool ) {
            state %cache;
            return @!buttons[$_] with %cache{$key};
            return @!buttons[ %cache{$key} = $_ - 1 ]
                with SDL::MouseButton::{ 'BUTTON_' ~ $key };
            die 'Unknown mouse button: ' ~ $key;
        }

        method !xy  { return-rw $!xy      }
        method !rel { return-rw $!rel     }
        method !whl { return-rw $!wheel   }
        method !btn { return-rw @!buttons }
    }

    my class Controller {
        trusts Pop::Inputs;

        has SDL::GameController $!sdl;
        has int  @!axes;
        has Bool @!buttons;

        method axis ( Int:D $index --> int ) { @!axes[ $index ] }

        method trigger ( Str:D $key, Bool :$raw --> Real ) {
            die 'Unknown joystick name: ' ~ $key unless $key eq 'LEFT' | 'RIGHT';
            my $value = @!axes[ $key eq 'LEFT' ?? 4 !! 5 ];
            $raw ?? $value !! $value / ( 1 +< 15 );
        }

        method joystick ( Str:D $key, Bool :$raw --> Pop::Point ) {
            die 'Unknown joystick name: ' ~ $key unless $key eq 'LEFT' | 'RIGHT';

            use Pop::Point :operators;
            my $point = Pop::Point.new(
                |@!axes[ $key eq 'LEFT' ?? |( 0, 1 ) !! |( 2, 3 ) ]
            );

            $raw ?? $point !! $point / ( 1 +< 15 );
        }

        multi method button ( Int:D $key --> Bool ) {
            @!buttons[ $key + 1 ]
        }
        multi method button ( SDL::GameControllerButton $key --> Bool ) {
            @!buttons[ $key + 1 ]
        }
        multi method button ( Str:D $key --> Bool ) {
            state %cache;
            return @!buttons[$_] with %cache{$key};
            return @!buttons[ %cache{$key} = $_ + 1 ]
                with SDL::GameControllerButton::{ 'CONTROLLER_BUTTON_' ~ $key };
            die 'Unknown controller button: ' ~ $key;
        }

        method !axs { return-rw @!axes    }
        method !btn { return-rw @!buttons }
        method !sdl { return-rw $!sdl     }
    }

    # Input callbacks
    has &!key-pressed;         method key-pressed         ( &!key-pressed         --> Nil ) { }
    has &!key-released;        method key-released        ( &!key-released        --> Nil ) { }
    has &!mouse-moved;         method mouse-moved         ( &!mouse-moved         --> Nil ) { }
    has &!wheel-moved;         method wheel-moved         ( &!wheel-moved         --> Nil ) { }
    has &!mouse-pressed;       method mouse-pressed       ( &!mouse-pressed       --> Nil ) { }
    has &!mouse-released;      method mouse-released      ( &!mouse-released      --> Nil ) { }
    has &!controller-moved;    method controller-moved    ( &!controller-moved    --> Nil ) { }
    has &!controller-pressed;  method controller-pressed  ( &!controller-pressed  --> Nil ) { }
    has &!controller-released; method controller-released ( &!controller-released --> Nil ) { }
    has &!file-dropped;        method file-dropped        ( &!file-dropped        --> Nil ) { }
    has &!text-dropped;        method text-dropped        ( &!text-dropped        --> Nil ) { }
    has &!text-input;          method text-input          ( &!text-input          --> Nil ) { }
    has &!text-edited;         method text-edited         ( &!text-edited         --> Nil ) { }

    # Window callbacks
    has &!focused;             method focused             ( &!focused             --> Nil ) { }
    has &!mouse-focused;       method mouse-focused       ( &!mouse-focused       --> Nil ) { }
    has &!window-exposed;      method window-exposed      ( &!window-exposed      --> Nil ) { }
    has &!window-moved;        method window-moved        ( &!window-moved        --> Nil ) { }
    has &!window-resized;      method window-resized      ( &!window-resized      --> Nil ) { }
    has &!window-visible;      method window-visible      ( &!window-visible      --> Nil ) { }

    has Mouse $!mouse;
    method mouse { $!mouse }

    has Bool @!keyboard;
    multi method keyboard ( SDL::Scancode $key --> Bool ) {
        @!keyboard{$key}
    }
    multi method keyboard ( SDL::Keycode $key --> Bool ) {
        state %cache;
        @!keyboard[ %cache{$key} //= SDL::GetScancodeFromKey( +$key ) ]
    }
    multi method keyboard ( Int:D $key, Bool :$scancode --> Bool ) {
        state %cache;
        @!keyboard[ $scancode
            ?? $key
            !! %cache{$key} //= SDL::GetScancodeFromKey($key)
        ]
    }
    multi method keyboard ( Str:D $key, Bool :$scancode where *.so --> Bool ) {
        state %cache;
        return @!keyboard[$_] with %cache{$key};
        return @!keyboard[ %cache{$key} = $_ ]
            with SDL::Scancode::{ 'SCANCODE_' ~ $key };
        die 'Unknown controller button: ' ~ $key;
    }
    multi method keyboard ( Str:D $key, Bool :$scancode where *.not --> Bool ) {
        state %cache;
        return @!keyboard[$_] with %cache{$key};
        return @!keyboard[ %cache{$key} = SDL::GetScancodeFromKey( +$_ ) ]
            with SDL::Keycode::{ 'K_' ~ $key };
        die 'Unknown controller button: ' ~ $key;
    }

    has Array of Controller $!controller;
    method controller ( Int:D $key ) { $!controller[ $key ] //= Controller.new }

    submethod TWEAK {
        my %config = Pop::Config.get: 'inputs';
        for < mouse keyboard controller > -> $name {
            if %config{$name} {
                .set_value( self, .get_value(self).new )
                    with self.^attributes.first: *.name eq '$!' ~ $name;
                next;
            }

            self.^find_method($name).wrap: {
                die "{ $name.wordcase } not enabled. Enable inputs.$name";
            }
        }

        SDL::EventState( SDL::WINDOWEVENT, +.so ) given %config<window>;

        given %config<file-drop> -> $state {
            SDL::EventState( $_, +$state.so )
                for ( SDL::DROPFILE, SDL::DROPTEXT, SDL::DROPBEGIN, SDL::DROPCOMPLETE );
        }

        if $!controller.defined {
            SDL::InitSubSystem(SDL::INIT_GAMECONTROLLER)
                unless SDL::WasInit(SDL::INIT_GAMECONTROLLER);
        }
    }

    # Polls for input and updates internal status
    # Returns True unless QUIT was triggered
    method update of Bool {
        state $event = SDL::Event.new;

        while SDL::PollEvent($event) {
            given $event.type {
                when SDL::QUIT { last }
                when MOUSE-EVENT {
                    self!handle-mouse: $event if $!mouse.defined;
                }
                when KEYBOARD-EVENT {
                    self!handle-keyboard: $event if @!keyboard.defined;
                }
                when CONTROLLER-EVENT {
                    self!handle-controller: $event if $!controller.defined;
                }
                when SDL::WINDOWEVENT {
                    self!handle-window: $event.window;
                }
                when DROP-EVENT {
                    self!handle-file-drop: $event.drop;
                }
                when JOYSTICK-EVENT { } # Do nothing, handled as controller
                default {
                    note ( SDL::EventType($event.type) // 'UNKOWN' )
                        ~ ' event not handled: ' ~ $event.raku;
                }
            }
        }

        $event.type !~~ SDL::QUIT;
    }

    method !handle-mouse ( SDL::Event $e ) {
        state %keymap = SDL::MouseButton::.antipairs.map: { .key.Int => .value.subst: /^BUTTON_/ }

        given $e.type {
            when SDL::MOUSEMOTION {
                with $e.motion {
                    $.mouse!Mouse::xy  .= new( .x,    .y );
                    $.mouse!Mouse::rel .= new( .xrel, .yrel );
                    .( $!mouse.xy, $!mouse.rel ) with &!mouse-moved;
                }
            }
            when SDL::MOUSEBUTTONDOWN | SDL::MOUSEBUTTONUP {
                my $mouse = $e.button;
                $.mouse!Mouse::btn[ $mouse.button - 1 ] = $mouse.state.so;
                when SDL::MOUSEBUTTONUP   {
                    .( $!mouse.xy, %keymap{$mouse.button} ) with &!mouse-released
                }
                when SDL::MOUSEBUTTONDOWN {
                    .( $!mouse.xy, %keymap{$mouse.button} ) with &!mouse-pressed
                }
            }
            when SDL::MOUSEWHEEL {
                $.mouse!Mouse::whl .= new( .x, .y ) with $e.wheel;
                .( $!mouse.wheel ) with &!wheel-moved;
            }
            default {
                note ( SDL::EventType($e.type) // 'UNKOWN' )
                    ~ ' event not handled: ' ~ $e.raku;
            }
        }
    }

    method !handle-keyboard ( SDL::Event $e ) {
        state %keymap = SDL::Keycode::.antipairs.map: { .key.Int => .value.subst: /^K_/ }

        given $e.type {
            when SDL::TEXTINPUT {
                .( $e.text.text ) with &!text-input
            }
            when SDL::TEXTEDITING {
                with &!text-edited {
                    my $edit = $e.edit;
                    .( $edit.text, $edit.start, $edit.length );
                }
            }
            when SDL::KEYUP | SDL::KEYDOWN {
                my $k = $e.key;
                @!keyboard[ $k.scancode ] = $k.state.so;

                when SDL::KEYUP {
                    .( %keymap{$k.sym}, $k.scancode ) with &!key-released
                }
                when SDL::KEYDOWN {
                    .( %keymap{$k.sym}, $k.scancode, $k.repeat.so ) with &!key-pressed
                }
            }
            default {
                note ( SDL::EventType($e.type) // 'UNKOWN' )
                    ~ ' event not handled: ' ~ $e.raku;
            }
        }
    }

    method !handle-controller ( SDL::Event $e ) {
        state %keymap = SDL::GameControllerButton::.antipairs.map: {
            .key.Int => .value.subst: /^CONTROLLER_BUTTON_/
        }

        given $e.type {
            when SDL::CONTROLLERAXISMOTION {
                my $axis = $e.caxis;

                # FIXME: Only one controller supported
                $.controller(0)!Controller::axs[ $axis.axis ] = $axis.value;

                .( $.controller(0), $axis.axis, $axis.value ) with &!controller-moved
            }
            when SDL::CONTROLLERBUTTONDOWN | SDL::CONTROLLERBUTTONUP {
                my $btn = $e.cbutton;

                # FIXME: Only one controller supported
                $.controller(0)!Controller::btn[ $btn.button + 1 ] = $btn.state.so;

                when SDL::CONTROLLERBUTTONDOWN {
                    .( $.controller(0), %keymap{$btn.button} ) with &!controller-pressed
                }
                when SDL::CONTROLLERBUTTONUP {
                    .( $.controller(0), %keymap{$btn.button} ) with &!controller-released
                }
            }
            when SDL::CONTROLLERDEVICEADDED {
                with $e.cdevice {
                    my $i = .which;
                    note "A controller has been added";

                    if SDL::IsGameController($i) {
                        with SDL::GameControllerOpen($i) -> $pad {
                            my $index = SDL::JoystickInstanceID(
                                SDL::GameControllerGetJoystick($pad) );

                            note sprintf Q[Opened controller %i (%s)], $index,
                                SDL::GameControllerNameForIndex($i) || '???';

                            # FIXME: Only one controller supported
                            $.controller(0)!Controller::sdl = $pad;
                        }
                        else {
                            note "Could not open controller: { SDL::GetError }";
                        }
                    }
                }
            }
            when SDL::CONTROLLERDEVICEREMOVED {
                with $e.cdevice {
                    my $index = .which;
                    note "Controller $index has been removed";

                    # FIXME: Only one controller supported
                    with $.controller(0) {
                        SDL::GameControllerClose($_!Controller::sdl);
                    }
                    else {
                        note 'Not a known controller!';
                    }
                }
            }
            when SDL::CONTROLLERDEVICEREMAPPED {
                note 'Remapped';
                dd $e.cdevice;
            }
            default {
                note ( SDL::EventType($e.type) // 'UNKOWN' )
                    ~ ' event not handled: ' ~ $e.raku;
            }
        }
    }

    method !handle-window ( SDL::WindowEvent $e ) {
        given $e.event {
            when SDL::WINDOWEVENT_SHOWN {
                # note "Window { $e.window-id } shown";
                .( True ) with &!window-visible;
            }
            when SDL::WINDOWEVENT_HIDDEN {
                # note "Window { $e.window-id } hidden";
                .( False ) with &!window-visible;
            }
            when SDL::WINDOWEVENT_EXPOSED {
                # note "Window { $e.window-id } exposed";
                .() with &!window-exposed;
            }
            when SDL::WINDOWEVENT_MOVED {
                # note "Window { $e.window-id } moved to ( { $e.data1 }, { $e.data2 })";
                .( Pop::Point.new( $e.data1, $e.data2 ) ) with &!window-moved;
            }
            when SDL::WINDOWEVENT_RESIZED {
                # note "Window { $e.window-id } resized to { $e.data1 } x { $e.data2 }";
                .( Pop::Point.new( $e.data1, $e.data2 ) ) with &!window-resized;
            }
            when SDL::WINDOWEVENT_SIZE_CHANGED {
                # note "Window { $e.window-id } size changed to { $e.data1 } x { $e.data2 }";
                .( Pop::Point.new( $e.data1, $e.data2 ) ) with &!window-resized;
            }
            when SDL::WINDOWEVENT_MINIMIZED { # Handled by HIDDEN
                # note "Window { $e.window-id } minimized";
            }
            when SDL::WINDOWEVENT_MAXIMIZED {
                note "Window { $e.window-id } maximized";
            }
            when SDL::WINDOWEVENT_RESTORED { # Handled by SHOWN
                # note "Window { $e.window-id } restored";
            }
            when SDL::WINDOWEVENT_ENTER {
                # note "Window { $e.window-id } mouse entered";
                .( True ) with &!mouse-focused;
            }
            when SDL::WINDOWEVENT_LEAVE {
                # note "Window { $e.window-id } mouse left";
                .( False ) with &!mouse-focused;
            }
            when SDL::WINDOWEVENT_FOCUS_GAINED {
                # note "Window { $e.window-id } gained keyboard focus";
                .( True ) with &!focused;
            }
            when SDL::WINDOWEVENT_FOCUS_LOST {
                # note "Window { $e.window-id } lost keyboard focus";
                .( False ) with &!focused;
            }
            when SDL::WINDOWEVENT_CLOSE {
                # note "Window { $e.window-id } closed";
            }
            when SDL::WINDOWEVENT_TAKE_FOCUS {
                # note "Window { $e.window-id } is offered focus";
            }
            # when SDL::WINDOWEVENT_HIT_TEST {
            #     note "Window { $e.window-id } has a special hit test";
            # }
            default {
                note ( SDL::WindowEventType($e.event) // 'UNKOWN' )
                    ~ ' event not handled: ' ~ $e.raku;
            }
        }
    }

    method !handle-file-drop ( SDL::DropEvent $e ) {
        given $e.type {
            when SDL::DROPFILE {
                .( $e.file.IO ) with &!file-dropped;
            }
            when SDL::DROPTEXT {
                .( $e.file ) with &!text-dropped;
            }
            when SDL::DROPBEGIN | SDL::DROPCOMPLETE {
                # TODO: Not handling DROPBEGIN or DROPCOMPLETE for now
            }
            default {
                note ( SDL::EventType($e.type) // 'UNKOWN' )
                    ~ ' event not handled: ' ~ $e.raku;
            }
        }
    }
}
