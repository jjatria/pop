use Color;
unit class Pop::Color:ver<0.0.4>:auth<zef:jjatria> is Color;

multi method COERCE ( Str:D $spec ) { self.new: $spec }

multi method COERCE ( @ ( Int $r, Int $g, Int $b, Int $a = 0xFF ) ) {
    self.new: rgba => ( $r, $g, $b, $a )
}
