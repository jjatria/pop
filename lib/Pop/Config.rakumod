use Pop::Singleton;
unit class Pop::Config:ver<0.0.4>:auth<zef:jjatria> is Singleton;

use TOML::Thumb;
use Hash::Merge;
use Pop::SDL;

constant DEFAULTS = {
    window => {
        title      => 'Pop!',
        width      => 800,
        height     => 600,
        fullscreen => False,
        flags      => SDL::WINDOW_OPENGL,
    },
    inputs => {
        mouse      => True,
        keyboard   => True,
        controller => True,
        window     => True,
        file-drop  => True,
    },
};

has %!config is built = DEFAULTS;

submethod TWEAK ( IO() $path = $*PROGRAM.sibling('./config.toml') ) {
    %!config = merge-hash %!config, from-toml slurp $path if $path.e;
}

method get ( *@parts ) {
    my $node = %!config;
    $node = $node{$_} for @parts;
    $node;
}
