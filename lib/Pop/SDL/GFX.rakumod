unit module SDL::GFX:ver<0.0.4>:auth<zef:jjatria>;

use NativeCall;

use Pop::SDL;

my constant LIB = 'SDL2_gfx';

# Primitives

## Pixel

our sub pixelColor ( SDL::Renderer $rdr, int16 $x, int16 $y, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub pixelRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Horizontal line

our sub hlineColor ( SDL::Renderer $rdr, int16 $x1, int16 $x2, int16 $y, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub hlineRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $x2, int16 $y, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Vertical line

our sub vlineColor ( SDL::Renderer $rdr, int16 $x, int16 $y1, int16 $y2, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub vlineRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y1, int16 $y2, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Rectangle

our sub rectangleColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub rectangleRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Rounded-Corner Rectangle

our sub roundedRectangleColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $rad, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub roundedRectangleRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $rad, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

# Filled rectangle (Box)

our sub boxColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub boxRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Rounded-Corner Filler rectangle (Box)

our sub roundedBoxColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $rad, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub roundedBoxRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $rad, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Line

our sub lineColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub lineRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## AA Line

our sub aalineColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub aalineRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Thick Line

our sub thickLineColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint8 $w, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub thickLineRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, uint8 $w, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Circle

our sub circleColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub circleRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Arc

our sub arcColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, int16 $start, int16 $end, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub arcRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, int16 $start, int16 $end, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## AA Circle

our sub aacircleColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub aacircleRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Filled circle

our sub filledCircleColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub filledCircleRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Ellipse

our sub ellipseColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rx, int16 $ry, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub ellipseRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rx, int16 $ry, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## AA Ellipse

our sub aaellipseColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rx, int16 $ry, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub aaellipseRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rx, int16 $ry, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Filled Ellipse

our sub filledEllipseColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rx, int16 $ry, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub filledEllipseRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rx, int16 $ry, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Pie

our sub pieColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, int16 $start, int16 $end, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub pieRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, int16 $start, int16 $end, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Filled Pie

our sub filledPieColor ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, int16 $start, int16 $end, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub filledPieRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, int16 $rad, int16 $start, int16 $end, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Trigon

our sub trigonColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $x3, int16 $y3, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub trigonRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $x3, int16 $y3, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## AA Trigon

our sub aatrigonColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $x3, int16 $y3, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub aatrigonRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $x3, int16 $y3, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Filled Trigon

our sub filledTrigonColor ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $x3, int16 $y3, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub filledTrigonRGBA ( SDL::Renderer $rdr, int16 $x1, int16 $y1, int16 $x2, int16 $y2, int16 $x3, int16 $y3, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Polygon

our sub polygonColor ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub polygonRGBA ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## AA Polygon

our sub aapolygonColor ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub aapolygonRGBA ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Filled Polygon

our sub filledPolygonColor ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub filledPolygonRGBA ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Textured Polygon (untested)

our sub texturedPolygon ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, SDL::Surface $surface, int32 $dx, int32 $dy )
    returns int32
    is native(LIB) {*}

## Bezier

our sub bezierColor ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, int32 $s, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub bezierRGBA ( SDL::Renderer $rdr, CArray[int16] $vx, CArray[int16] $vy, int32 $n, int32 $s, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

## Characters/Strings

# untested
our sub gfxPrimitivesSetFont ( Pointer $fontdata, uint32 $cw, uint32 $ch )
    is native(LIB) {*}

our sub gfxPrimitivesSetFontRotation ( uint32 $rotation )
    is native(LIB) {*}

our sub characterColor ( SDL::Renderer $rdr, int16 $x, int16 $y, uint8 $char, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub characterRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, uint8 $char, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

our sub stringColor ( SDL::Renderer $rdr, int16 $x, int16 $y, Str $s, uint32 $color )
    returns int32
    is native(LIB) {*}

our sub stringRGBA ( SDL::Renderer $rdr, int16 $x, int16 $y, Str $s, uint8 $r, uint8 $g, uint8 $b, uint8 $a )
    returns int32
    is native(LIB) {*}

# Rotozoom

our sub rotozoomSurface ( SDL::Surface $src, num64 $angle, num64 $zoom, int8 $smooth )
    returns SDL::Surface
    is native(LIB) {*}

our sub rotozoomSurfaceXY ( SDL::Surface $src, num64 $angle, num64 $zoomx, num64 $zoomy, int8 $smooth )
    returns SDL::Surface
    is native(LIB) {*}

our sub rotozoomSurfaceSize ( int32 $width, int32 $height, num64 $angle, num64 $zoom, int32 $dstwidth is rw, int32 $dstheight is rw )
    is native(LIB) {*}

our sub rotozoomSurfaceSizeXY ( int32 $width, int32 $height, num64 $angle, num64 $zoomx, num64 $zoomy, int32 $dstwidth is rw, int32 $dstheight is rw )
    is native(LIB) {*}

our sub zoomSurface ( SDL::Surface $src, num64 $zoomx, num64 $zoomy, int8 $smooth )
    returns SDL::Surface
    is native(LIB) {*}

our sub zoomSurfaceSize ( int32 $width, int32 $height, num64 $zoomx, num64 $zoomy, int32 $dstwidth is rw, int32 $dstheight is rw )
    is native(LIB) {*}

our sub shrinkSurface ( SDL::Surface $src, num64 $factorx, num64 $factory )
    returns SDL::Surface
    is native(LIB) {*}

our sub rotateSurface90Degrees ( SDL::Surface $src, int8 $numturns )
    returns SDL::Surface
    is native(LIB) {*}
