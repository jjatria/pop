use Pop::Singleton;
unit class Pop::Graphics:ver<0.0.4>:auth<zef:jjatria> is Singleton;

use Pop::Config;
use Pop::Images;
use Pop::Point;
use Pop::Rect;
use Pop::Drawable;
use Pop::Texture;

use Pop::SDL;
use Pop::SDL::GFX;

has Pop::Point     $!size handles ( width => 'x', height => 'y' ) .= zero;
has SDL::Window   $.window;
has SDL::Renderer $.renderer;
has Pop::Point     $.offset is rw .= zero;

submethod TWEAK (
    :$width      is copy,
    :$height     is copy,
    :$title      is copy,
    :$fullscreen is copy,
    :$flags      is copy,
) {
    if SDL::Init(SDL::INIT_VIDEO) {
        note "Error initialising SDL: { SDL::GetError() }";
    }

    with Pop::Config.get: 'window' {
        $height     //= .<height>;
        $width      //= .<width>;
        $title      //= .<title>;
        $fullscreen //= .<fullscreen>;
        $flags      //= .<flags>;
    }

    $flags = $flags +| ( SDL::WINDOW_FULLSCREEN_DESKTOP if $fullscreen );

    $!window = SDL::CreateWindow(
        $title,
        SDL::WINDOWPOS_CENTERED,
        SDL::WINDOWPOS_CENTERED,
        $width,
        $height,
        $flags,
    ) or note "Error creating SDL window: { SDL::GetError() }";

    #                                       Index      Flags
    #                                            \    /
    $!renderer = SDL::CreateRenderer( $!window, -1, 0 )
        or note "Error creating SDL renderer: { SDL::GetError() }";

    SDL::RenderSetLogicalSize $!renderer, $width, $height if $fullscreen;

    $!size .= add: $width, $height;
}

method set-target ( Pop::Texture $texture --> Nil ) {
    SDL::SetRenderTarget( $!renderer, $texture.texture )
        and die "Error setting render target: { SDL::GetError() }";
}

method reset-target ( --> Nil ) {
    SDL::SetRenderTarget( $!renderer, SDL::Texture )
        and die "Error resetting render target: { SDL::GetError() }";
}

method cursor-visibility ( Bool() $new-state? --> Bool ) {
    my $out = SDL::ShowCursor( $new-state.defined ?? $new-state.Int !! -1 );
    die "Error querying cursor state: { SDL::GetError() }" if $out < 0;
    so $out;
}

# Multi draw method

# There are two variants for the draw method: a high-level one, which should
# be the most convenient to use in most cases; and a low-level one for users
# who would rather sacrifice usability for performance.

# The main version of the draw method is the high-level one, which will be
# the one you'll most likely be using. It takes two mandatory parameters:
# a Pop::Drawable object to draw, and a coordinate in the screen to draw it in.
#
# The coordinate will correspond to the place where the top-left corner of the
# Pop::Drawable object will be drawn. This can be adjusted with the ':offset'
# named parameter. The value of this parameter must be coerceable to a point,
# where each component will be interpreted as a ratio along each axes, with 0
# being the top / left end, and 1 being the bottom / right end.
#
# A ':scale' parameter (also coerceable to a Pop::Point) will be used to scale
# the image when drawing. Setting this to eg. ( 0.5, 0.5 ) would mean that the
# object should be drawn at half its size. The value in each component will be
# applied independently to scale the respective axis.
#
# To flip the image vertically and / or horizontally when drawing, the ':flipx'
# and ':flipy' flags can be set respectively. To flip both at once, ':flipxy'
# is also available.
#
# An rotation angle can be specified with the ':angle' parameter. The pivot
# point for this rotation will default to being the centre of the image to
# render. Like with the above, this should be coerceable to a Pop::Point
# object, with values between 0 and 1 indicating relative positions.
#
# By default, the image to draw will be clipped using the result of calling
# the `.clip` method on the Pop::Drawable object. This can be overriden for
# convenience by passing a value that is coerceable to a Pop::Rect object as
# the ':clip' parameter.
#
multi method draw (
    Pop::Drawable:D $drawable,        #= The object to render
    Pop::Point()    $xy,              #= The point in the screen where to draw
    Pop::Point()   :$offset,          #= The offset of the drawing coordinates
    Pop::Point()   :$scale,           #= The X and Y scale for drawing
    Pop::Rect()    :$clip,            #= An optional clipping rectangle
    Pop::Point()   :$pivot,           #= The pivot point to use when rotating
    Num(Numeric)   :$angle = 0,       #= The angle to use for rotation
    Bool           :$flipxy,          #= Set to true to flip along both axes
    Bool           :$flipx = $flipxy, #= Set to true to flip horizontally
    Bool           :$flipy = $flipxy, #= Set to true to flip vertically
    --> Nil
) {
    my $flip = SDL::FLIP_NONE;
    $flip = $flip +| SDL::FLIP_HORIZONTAL if $flipx;
    $flip = $flip +| SDL::FLIP_VERTICAL   if $flipy;

    my $src = ( $clip // $drawable.clip );

    my $dst-xy = $xy.add: $!offset;
    $dst-xy .= sub: .mul( $src.wh ) with $offset;

    my $dst-wh = $src.wh;
    $dst-wh .= mul: $_ with $scale;

    my $dst = $xy.defined ?? SDL::Rect.new( |$dst-xy, |$dst-wh ) !! SDL::Rect;

    if $angle || $flip {
        SDL::RenderCopyEx(
            $!renderer, $drawable.texture, $src.sdl, $dst, $angle,
            ( $pivot ?? $pivot.mul( $dst-wh ) !! $pivot ).sdl, $flip
        )
    }
    else {
        SDL::RenderCopy( $!renderer, $drawable.texture, $src.sdl, $dst )
    }
}

# The lowest-level version takes direct SDL representations

multi method draw (
    SDL::Texture:D  $texture,
    SDL::Rect:D     $s,
    SDL::Rect:D     $dst,
    Numeric         :$angle = 0,
    SDL::Point     :$pivot,
    Int             :$flip = SDL::FLIP_NONE,
    --> Nil
) {
    my $src = not $!offset ?? $s
        !! SDL::Rect.new( |$!offset.add( $s.x, $s.y ), $s.w, $s.h );

    if $angle || $flip {
        SDL::RenderCopyEx( $!renderer, $texture, $src, $dst,
            $angle, $pivot, $flip )
    }
    else {
        SDL::RenderCopy( $!renderer, $texture, $src, $dst )
    }
}

multi method clear ( Pop::Color:D() $bg --> Nil ) {
    samewith( $bg // Pop::Color.new: 0, 0, 0, 255 )
}

multi method clear (
    Int:D $r = 0,
    Int:D $g = 0,
    Int:D $b = 0,
    Int:D $a = 255
    --> Nil
) {
    SDL::SetRenderDrawColor( $!renderer, $r, $g, $b, $a );
    SDL::RenderClear($!renderer);
}

method present ( --> Nil ) { SDL::RenderPresent($!renderer) }

method destroy ( --> Nil ) {
    with $!renderer {
        note 'Destroying renderer';
        SDL::DestroyRenderer($_);
        $_ = Nil;
    }

    with $!window {
        note 'Destroying window';
        SDL::DestroyWindow($_);
        $_ = Nil;
    }
}

# Drawing methods

## Points

method point ( Pop::Point:D() $xy, Pop::Color:D() $color --> Nil ) {
    SDL::GFX::pixelRGBA( $!renderer, |$xy.add( $!offset )».Int, |$color.rgba )
}

## Lines

method line (
    Pop::Point:D() $xy1 is copy,
    Pop::Point:D() $xy2 is copy,
    Pop::Color:D() $color,

    Bool :aa(:$anti-alias),
    Int      :$width,
    --> Nil
) {
    if $!offset {
        $xy1 .= add: $!offset;
        $xy2 .= add: $!offset;
    }

    when $xy1.y == $xy2.y {
        SDL::GFX::hlineRGBA( $!renderer, $xy1.x.Int, |$xy2».Int, |$color.rgba )
    }
    when $xy1.x == $xy2.y {
        SDL::GFX::vlineRGBA( $!renderer, |$xy1».Int, $xy2.y.Int, |$color.rgba )
    }
    when $anti-alias {
        SDL::GFX::aalineRGBA( $!renderer, |$xy1».Int, |$xy2».Int, |$color.rgba )
    }
    when $width.defined {
        SDL::GFX::thickLineRGBA(
            $!renderer, |$xy1».Int, |$xy2».Int, $width, |$color.rgba )
    }
    SDL::GFX::lineRGBA( $!renderer, |$xy1».Int, |$xy2».Int, |$color.rgba )
}

## Rectangles

multi method rectangle ( Pop::Rect:D $r, |c --> Nil ) {
    samewith( $r.xy, $r.xy.add( $r.wh ), |c )
}

multi method rectangle (
    Pop::Point:D() $xy1 is copy,
    Pop::Point:D() $xy2 is copy,
    Pop::Color:D() $color,

    Bool :$fill,
    Int  :border-radius($radius),
    --> Nil
) {
    if $!offset {
        $xy1 .= add: $!offset;
        $xy2 .= add: $!offset;
    }

    when $fill && $radius.defined {
        SDL::GFX::roundedBoxRGBA(
            $!renderer, |$xy1».Int, |$xy2».Int, $radius, |$color.rgba )
    }
    when $fill {
        SDL::GFX::boxRGBA( $!renderer, |$xy1».Int, |$xy2».Int, |$color.rgba )
    }
    when $radius.defined {
        SDL::GFX::roundedRectangleRGBA(
            $!renderer, |$xy1».Int, |$xy2».Int, $radius, |$color.rgba )
    }
    SDL::GFX::rectangleRGBA( $!renderer, |$xy1».Int, |$xy2».Int, |$color.rgba )
}

## Circles

method circle (
    Pop::Point:D() $centre is copy,
    Int:D()        $radius,
    Pop::Color:D() $color,

    Bool :aa(:$anti-alias),
    Bool     :$fill,
    --> Nil
) {
    $centre .= add: $!offset if $!offset;

    when so $fill {
        SDL::GFX::filledCircleRGBA( $!renderer, |$centre».Int, $radius, |$color.rgba )
    }
    when so $anti-alias {
        SDL::GFX::aacircleRGBA( $!renderer, |$centre».Int, $radius, |$color.rgba )
    }
    SDL::GFX::circleRGBA( $!renderer, |$centre».Int, $radius, |$color.rgba )
}

## Arcs

multi method arc (
    Pop::Point:D() $xy, # Centre

    Int:D() $radius,
    Int:D() $start,
    Int:D() $end,

    Pop::Color:D() $color,

    Bool :$pie,
    Bool :$fill,
    --> Nil
) {
    $xy .= add: $!offset if $!offset;

    when $fill && $pie {
        SDL::GFX::filledPieRGBA(
            $!renderer, |$xy».Int, $radius, $start, $end, |$color.rgba )
    }
    when so $pie {
        SDL::GFX::pieRGBA(
            $!renderer, |$xy».Int, $radius, $start, $end, |$color.rgba )
    }
    SDL::GFX::arcRGBA( $!renderer, |$xy».Int, $radius, $start, $end, |$color.rgba )
}

## Ellipses

method ellipse (
    Pop::Point:D() $centre is copy, # Centre
    Pop::Point:D() $radii,          # Horizontal and vertical radii
    Pop::Color:D() $color,

    Bool :aa(:$anti-alias),
    Bool     :$fill,
    --> Nil
) {
    $centre .= add: $!offset if $!offset;

    when so $fill {
        SDL::GFX::filledEllipseRGBA(
            $!renderer, |$centre».Int, |$radii».Int, |$color.rgba )
    }
    when so $anti-alias {
        SDL::GFX::aaellipseRGBA(
            $!renderer, |$centre».Int, |$radii».Int, |$color.rgba )
    }
    SDL::GFX::ellipseRGBA(
        $!renderer, |$centre».Int, |$radii».Int, |$color.rgba )
}

## Polygons

method polygon (
    @points is copy,
    Pop::Color:D() $color,
    Bool :aa(:$anti-alias),
    Bool     :$fill,
    --> Nil
) {
    my $n = @points.elems;
    @points .= map: { $!offset.add: |$_ } if $!offset;

    when $n != 3 {
        use NativeCall;

        my $vx = CArray[int16].allocate($n);
        my $vy = CArray[int16].allocate($n);

        ( $vx[$_], $vy[$_] ) = |@points[$_]».Int for ^$n;

        when so $fill {
            SDL::GFX::filledPolygonRGBA( $!renderer, $vx, $vy, $n, |$color.rgba )
        }
        when so $anti-alias {
            SDL::GFX::aapolygonRGBA( $!renderer, $vx, $vy, $n, |$color.rgba )
        }
        SDL::GFX::polygonRGBA( $!renderer, $vx, $vy, $n, |$color.rgba )
    }
    when so $fill {
        SDL::GFX::filledTrigonRGBA(
            $!renderer, |@points».list.flat».Int, |$color.rgba )
    }
    when so $anti-alias {
        SDL::GFX::aatrigonRGBA(
            $!renderer, |@points».list.flat».Int, |$color.rgba )
    }
    SDL::GFX::trigonRGBA( $!renderer, |@points».list.flat».Int, |$color.rgba )
}

## Beziers

method bezier (
    @points is copy,
    Pop::Color:D() $color,
    Int :$steps where * >= 2 = 2,
    --> Nil
) {
    use NativeCall;

    my $n = @points.elems;
    @points .= map: { $!offset.add: |$_ } if $!offset;

    my $vx = CArray[int16].allocate($n);
    my $vy = CArray[int16].allocate($n);

    ( $vx[$_], $vy[$_] ) = |@points[$_]».Int for ^$n;

    SDL::GFX::bezierRGBA( $!renderer, $vx, $vy, $n, $steps, |$color.rgba )
}

# Strings

multi method string (
    Str:D() $string,
    Pop::Point:D() $xy is copy,
    Pop::Color:D() $color,
    --> Nil
) {
    $xy .= add: $!offset if $!offset;
    SDL::GFX::stringRGBA( $!renderer, |$xy».Int, $string, |$color.rgba )
}
