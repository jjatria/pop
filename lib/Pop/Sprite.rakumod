use Pop::Drawable;
unit class Pop::Sprite:ver<0.0.4>:auth<zef:jjatria> does Pop::Drawable;

has $!start = now;
has $.fps   = 0;
has @.frames;

# The clipping rectangle of a sprite moves across frames according to the
# sprite's animation mode. Currently, only looping animations are supported.
method clip { @!frames[ ( ( now - $!start ) * $!fps ).Int % @!frames ] }
