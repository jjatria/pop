=begin pod

=head1 NAME

Pop

=head1 SYNOPSIS

    use Pop;
    use Pop::Inputs;
    use Pop::Graphics;

    # Create your entities, maybe with Pop::Entities

    Pop.new: title => 'Revenge of the Avengers';

    Pop.update: -> $delta-in-seconds {
        # Run update systems

        # Inputs are automatically read at the beginning
        # of the main loop and exposed through Pop::Inputs
        Pop.stop if Pop::Inputs.keyboard: 'ESCAPE';
    }

    Pop.render: {
        # Run rendering systems.

        # Screen buffers will be automatically swapped after this block

        given Pop::Graphics {
            .point: ( (^.width).pick, (^.height).pick ), (^256).pick xx 3;
        }
    }

    # Run the main loop
    Pop.run;

=head1 DESCRIPTION

Pop is an experimental 2D game development framework for Raku. It has been
inspired by frameworks like LÖVE2D and Pico-8, and by others of a similar
nature. It runs on SDL2 using custom bindings.

This project is still in its infancy, and the interface might change without
warning. Any changes (breaking or otherwise) will still be reported in the
change log, so that is the first place to check before updating.

=head2 External Dependencies

Since most of the heavy lifting is done using SDL2, Pop requires some external
libraries in order to work. In particular, it requires the following libraries
to be available in your system:

=item SDL2
=item SDL2_image
=item SDL2_gfx

Please refer to your system's documentation for details on how to obtain
these.

=head1 CONCEPTS

=head2 Singletons

Most core classes (including the main Pop game class as well as helpers like
Pop::Graphics and Pop::inputs) are implemented as globally available
singletons. This means that, unless specific parameters need to be set during
initialisation, there is no need to manually construct them and pass
references to them, which should help simplify game code.

The caveat to this is that, since they will be constructed automatically if
needed, any manual initialisation needs to be done as early as possible.

=head2 Configuration

Most default values used throughout Pop can be specified via the global
Pop::Config singleton (see the documentation of that module for more details).

The values in Pop::Config will be read from a file named C<config.toml> in the
same directory as the mainline entrypoint. To read the global configuration
from a different path, pass that path to C<Pop::Config.new> as early as
possible.

=head1 METHODS

=head2 new

    method new (|c) returns Pop

Initialises a new global game object.

This function will be called automatically when any method is called on
C<Pop>, so it should be called as early as possible unless the default values
are appropriate.

The parameters passed in will be forwarded to the initialisation of the global
L<Pop::Graphics> singleton. See that module for more details about acceptable
parameters and their meanings.

Default values are those specified by L<Pop::Config>. See that module for more
details.

=head2 run

    method new (
        :$fps = 60,
        :$dt,
    ) returns Nil

Starts the execution a game's main loop. The main loop will continue to
execute until C<stop> is called, either directly or through C<handle-input>.

The default main loop has roughly the following stages:

=item C<handle-input> is called, to process input events.

=begin item
The C<update> block is executed as many times as needed.

If the C<$dt> parameter was set, then the block will be called as long as that
amount of time has accumulated since the last time it was called. Otherwise,
the block will be called once with however much time has elapsed since the
last frame.
=end item

=begin item
The C<render> block gets called, as long as the time spent on the frame is not
already longer than the time needed to maintain the framerate specified in the
C<$fps> parameter (which defaults to 60).

If the C<$fps> parameter was explicitly disabled (eg. with C<:!fps>) then the
C<render> block will be called once per frame.
=end item

=head2 stop

    method stop () returns Nil

Stop execution of a game, which started the last time C<run> was called.

=head2 handle-input

    method handle-input () returns Nil

Process input events and update the state of Pop::Inputs. See the
documentation for L<Pop::Inputs.update> for more details.

Calling C<handle-input> will automatically call C<stop> when a QUIT event
is received (ie. when the game window is closed).

=head2 delta

    method delta () returns Duration

Returns a Duration object representing the time elapsed since the last frame
started.

=head2 wait

    method wait (
        Int :$dt,
    ) returns Nil

Stop execution for at least the specified number of seconds.

=head2 destroy

    method destroy () returns Nil

Frees any resources that have been loaded and cached by the game.

=head1 MAIN CALLBACKS

=head2 update

    method update (
        &update,
    ) returns Nil

Set a code block to be run to update the game state on every game frame. The
code will be called with the delta time in seconds as the only parameter.

The C<update> block runs before the C<render> block (see below), possibly
multiple times per frame, depending on the values passed to C<run>. See the
entry for that method for details on the structure of the default main loop.

=head2 render

    method render (
        &render,
    ) returns Nil

Set a code block to be run to render the game state. This block will be called
up to once per frame, with no arguments.

For details on the aspects of the main loop that control whether the C<render>
block gets called or not, see the entry for the C<run> method above.

=head1 INPUT CALLBACKS

The callbacks listed below are shortcuts for the callbacks of the same name in
Pop::Inputs. For more details, see that module's documentation.

=head2 key-pressed

    method key-pressed ( &cb ) returns Nil

=head2 key-released

    method key-released ( &cb ) returns Nil

=head2 mouse-moved

    method mouse-moved ( &cb ) returns Nil

=head2 wheel-moved

    method wheel-moved ( &cb ) returns Nil

=head2 mouse-pressed

    method mouse-pressed ( &cb ) returns Nil

=head2 mouse-released

    method mouse-released ( &cb ) returns Nil

=head2 controller-moved

    method controller-moved ( &cb ) returns Nil

=head2 controller-pressed

    method controller-pressed ( &cb ) returns Nil

=head2 controller-released

    method controller-released ( &cb ) returns Nil

=head2 file-dropped

    method file-dropped ( &cb ) returns Nil

=head2 text-dropped

    method text-dropped ( &cb ) returns Nil

=head2 text-input

    method text-input ( &cb ) returns Nil

=head2 text-edited

    method text-edited ( &cb ) returns Nil

=head1 WINDOW CALLBACKS

The callbacks listed below are shortcuts for the callbacks of the same name in
Pop::Inputs. For more details, see that module's documentation.

=head2 focused

    method focused ( &cb ) returns Nil

=head2 mouse-focused

    method mouse-focused ( &cb ) returns Nil

=head2 window-exposed

    method window-exposed ( &cb ) returns Nil

=head2 window-moved

    method window-moved ( &cb ) returns Nil

=head2 window-resized

    method window-resized ( &cb ) returns Nil

=head2 window-visible

    method window-visible ( &cb ) returns Nil

=head1 ACKNOWLEDGEMENTS

The core ECS logic in Pop::Entities has been heavily influenced, both in its
interface and its implementation, by L<EnTT|https://github.com/skypjack/entt>.

Some of the slicing of the singleton classes is based on that seen in popular
game development frameworks, but were most directly influenced by
L<LÖVE2D|https://love2d.org>.

The singleton role used in this distribution is also a greatly simplified
version of L<Staticish|https://github.com/jonathanstowe/Staticish>, while the
code in Pop::Point derives from a modified version of
L<Point|https://github.com/0racle/raku-Point>.

=head1 COPYRIGHT AND LICENSE

Copyright 2021 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under
the Artistic License 2.0.

=end pod
