#!/usr/bin/env raku

use lib 'lib';                    # For the Pop library
use lib $*PROGRAM.sibling('lib'); # For local lib

use Pop;
use Pop::Inputs;
use Pop::Entities;
use Local::Components;
use Local::Systems;

my ( $width, $height ) = ( 800, 600 );
my ( $axis-dirty, $button-dirty ) = True, True; # Dirty flags for rendering

create-buttons;

Pop.new: :$width, :$height, title => 'Gamepad demo';

Pop.key-pressed: -> $_, $, $ { Pop.stop when 'ESCAPE' }

Pop.controller-moved: -> $pad, $axis, $value {
    $axis-dirty = True;

    if $axis ~~ 0 .. 3 { # Joystick
        my $key = $axis == 0 | 1 ?? 'LEFT' !! 'RIGHT';
        Pop::Entities.view(Joystick).each: -> $c {
            next unless $c.key eq $key;
            $c.xy = $pad.joystick: $key;
        }
    }
    else { # Trigger
        my $key = $axis == 4 ?? 'LEFT' !! 'RIGHT';
        Pop::Entities.view(Trigger ).each: -> $c {
            next unless $c.key eq $key;
            $c.value = $pad.trigger:  $key;
        }

    }
}

Pop.controller-pressed: -> $, $key {
    $button-dirty = True;
    Pop::Entities.view(Button).each: -> $c {
        next unless $c.key eq $key;
        $c.pressed = True;
    }
}

Pop.controller-released: -> $, $key {
    $button-dirty = True;
    Pop::Entities.view(Button).each: -> $c {
        next unless $c.key eq $key;
        $c.pressed = False;
    }
}

Pop.render: { Systems::render( $axis-dirty, $button-dirty ) }

Pop.run;

# Entity factories

sub create-buttons {
    my $basey   = $height div 5;
    my $centrex = $width  div 2;
    my $centrey = $height div 2;

    with 0.5 * $centrex -> $x {
        my $y = 2 * $basey;
        make-dpad    'DPAD_DOWN',    $x,       $y + 40,  '#BABABAFF';
        make-dpad    'DPAD_RIGHT',   $x +  40, $y,       '#BABABAFF';
        make-dpad    'DPAD_LEFT',    $x -  40, $y,       '#BABABAFF';
        make-dpad    'DPAD_UP',      $x,       $y - 40,  '#BABABAFF';
        make-trigger 'LEFT',         $x - 100, $y - 80,  '#ABABABFF';
        make-button  'LEFTSHOULDER', $x,       $y div 2, '#ABABABFF';
    }

    with 1.5 * $centrex -> $x {
        my $y = 2 * $basey;
        make-button  'A',             $x,       $y + 40,  '#2FA01EFF';
        make-button  'B',             $x +  40, $y,       '#E5340AFF';
        make-button  'X',             $x -  40, $y,       '#003CBEFF';
        make-button  'Y',             $x,       $y - 40,  '#F2C813FF';
        make-trigger 'RIGHT',         $x +  80, $y - 80,  '#ABABABFF';
        make-button  'RIGHTSHOULDER', $x,       $y div 2, '#ABABABFF';
    }

    make-button 'BACK',  $centrex - 50, 3 * $basey, '#ABABABFF';
    make-button 'GUIDE', $centrex,      3 * $basey, '#ABABABFF';
    make-button 'START', $centrex + 50, 3 * $basey, '#ABABABFF';

    make-joystick 'LEFT',        $width div 3,   4 * $basey, '#ABABABFF';
    make-joystick 'RIGHT', 2 * ( $width div 3 ), 4 * $basey, '#ABABABFF';
}

sub base-button ( $name, $x, $y, $color ) {
    Pop::Entities.create:
        Position.new( $x, $y ),
        Coloured.new( :$color );
}

sub make-button ( $key, |c ) {
    Pop::Entities.add: $_,
        ActionButton.new,
        Button.new( :$key )
        with base-button $key.lc, |c;
}

sub make-dpad ( $key, |c ) {
    Pop::Entities.add: $_,
        DPad.new,
        Button.new( :$key )
        with base-button $key.lc, |c;
}

sub make-trigger ( $key, |c ) {
    Pop::Entities.add: $_,
        Trigger.new(:$key)
        with base-button "{ $key }-trigger".lc, |c;
}

sub make-joystick ( $key, |c ) {
    Pop::Entities.add: $_,
        Joystick.new(:$key),
        Button.new( key => $key ~ 'STICK' ),
        with base-button "{ $key }-joystick".lc, |c;
}
